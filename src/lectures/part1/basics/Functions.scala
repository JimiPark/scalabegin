package lectures.part1.basics

object Functions extends App {

  def aFunction(a: String, b: Int): String = {
    a + " " + b
  }

  println(aFunction("hello", 3))

  def aParameterlessFunction(): Int = 42
  println(aParameterlessFunction())

  def aRepeatedFunction(aString: String, n: Int): String = {
    if(n==1) aString
    else aString + aRepeatedFunction(aString, n-1)
  }
  println(aRepeatedFunction("MultiThread", 3))

  def aBigFunction(n: Int): Int = {
    def aSmallFunction(a: Int, b: Int): Int = a+b
    aSmallFunction(n, n-1)
  }

  def aGreetingFunction(aName: String, age: Int): String =
    "Hi, my name is "+ aName +" and I am "+ age +" years old"
  println(aGreetingFunction("Park", 36))

  def FactorialFunction(num:Int):Int={
    if(num <=0) 1
    else num * FactorialFunction(num-1)
  }
  println("Factorial result is : "+FactorialFunction(5))

  def Fibonacci (n: Int): Int =
    if (n<=2) 1
    else Fibonacci(n-1) + Fibonacci(n-2)

  println("Fibonacci result is :" + Fibonacci(8))

  def isPrime(n:Int): Boolean = {
    def isPrimeUntil(t:Int): Boolean =
      if(t<=1) true
      else n% t !=0 && isPrimeUntil(t-1)

    isPrimeUntil(n/2)
  }

  println(isPrime(37))
  println(isPrime(2003))
  println(isPrime(37*17))
}
