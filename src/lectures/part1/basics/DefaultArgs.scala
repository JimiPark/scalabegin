package lectures.part1.basics

object DefaultArgs extends App{

  def trFact(x: Int, acc: Int) : Int =
    if (x <=1) acc
    else trFact(x-1, x*acc)

  val fact10 = trFact(10, 1)

  def savePicture(format: String = "jpg", width: Int, height: Int): Unit = println("saving picture")
  savePicture(width=800, height = 600);

  savePicture(height = 800, width = 1080, format = "bmp")
}
