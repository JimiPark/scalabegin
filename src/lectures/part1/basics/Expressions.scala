package lectures.part1.basics

object Expressions extends  App{

  val x = 1 + 2 // EXPRESSION
  println(x)

  // Instructions (DO) vs Expressions (value)

  // IF expression

  var aVariable = 2
  aVariable += 3
  println(aVariable)

  val aCondition = true
  val aConditionedValue = if(aCondition) 5 else 3
  println(aConditionedValue)
  println(if(aCondition) 5 else 3)
  println(1+3)

  var i = 0
  while( i< 10){
    println(i)
    i += 1
  }

  // NEVER WRITE THIS AGAIN.
  // Everything in Scala is an Expression!

  val aWeirdValue = (aVariable = 3) // Unit === void
  println(aWeirdValue)

  val aCodeBlock = {
    val y = 2
    val z = y+1

    if(z>2) "Heloo" else "goodbye"
  }

  val someValue = {
    2 < 3
  }

  println(someValue)

  val someOtherValue = {
    if(someValue) 239 else 986
    42
  }

  println(someOtherValue)
}
