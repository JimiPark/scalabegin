package lectures.part1.basics

object ValuesVariableTypes extends App {

  val x: Int = 42;
  println(x);

  val aString: String = "hello"
  val anotherString = "Good bye"

  println(aString);
  println(anotherString)
  // VALS ARE IMMUTABLE

  //Types
  val aBoolean: Boolean = false;
  val aChar: Char = 'a'
  val anInt: Int = x
  val aShort: Short = 4613
  val aLong: Long = 52341234234L;
  val aFloat:Float = 2.0f;
  val aDouble: Double = 3.14;

  // variables.
  var aVariable: Int = 4;
  aVariable = 5 // side effects
  aVariable = 6;
  println(aVariable);
}
