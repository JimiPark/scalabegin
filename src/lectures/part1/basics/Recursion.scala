package lectures.part1.basics

import scala.annotation.tailrec
import scala.jdk.Accumulator

object Recursion extends App {

  def factorial (n: Int): Int = {
    if (n <= 1) 1
    else {
      println("computing factorial of " + n + " -1 first need factorail of " + (n-1))
      val result = n * factorial(n-1)
      println("Computed factorial of " + n) // using new stack

      result
    }
  }

  println(factorial(10))
//  println(factorial(5000)) this code call stackoverflow.

  def BigFactorial (n: Int) : BigInt = {
    @tailrec
    def FactHelper(x: Int, accumulator: BigInt) : BigInt =
      if(x<=1) accumulator
      else FactHelper(x-1, x*accumulator) // use previous stack.
    // Tail Recursion = use recursive call as the last expression.

    FactHelper(n,1)
  }

  println(BigFactorial(5000))

  // When you need loops, use_tail_recursion.

  /*
    1. Concatenate a string n times
    2. Isprime function tail recursive
    3. Fibonacci function, tail recursive.
  */

  def NTimeString(num: Int, aString: String, accumulator: String): String =
    if(num<=0) accumulator
    else NTimeString(num-1, aString, aString+" "+accumulator)

  println(NTimeString(3, "Scala", " "))

  def IsPrimeTail(n: Int) : Boolean = {
    @tailrec
    def isPrimeUntil(t: Int) : Boolean =
      if(t<=1) true
      else n% t !=0 && isPrimeUntil(t-1)

    isPrimeUntil(n/2)
  }

  println(IsPrimeTail(1))

  def Fibonacci(n: Int) : Int = {
    @tailrec
    def FiboTailrec(i: Int, last: Int, nextToLast: Int): Int =
      if(i>=n) last
      else FiboTailrec(i+1, last + nextToLast, last)

    if(n <= 2) 1
    else FiboTailrec(2, 1, 1)
  }

  println("Fibonacci: "+ Fibonacci(8))
//  def AccTest (n: Int) : Int = {
//    def AccNum(x: Int, accumulator: Int): Int =
//      if (x<=1) accumulator
//      else {
//        println("this turn x : " + x)
//        println("this accumulator: " + accumulator)
//        AccNum(x-1, accumulator+x)
//      }
//    AccNum(n, 0)
//  }
//
//  println(AccTest(3))

}
