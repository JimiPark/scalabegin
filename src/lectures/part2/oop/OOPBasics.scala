package lectures.part2.oop

import com.sun.jdi.connect.Connector.StringArgument
import javax.security.sasl.AuthorizeCallback

object OOPBasics extends App {

  val person = new Person("Jimi", 36)
  println(person.age)
  person.greet("Daniel")
  person.greet()

  val author = new Writer("Jimi", "Park", 1985)
  val imposter = new Writer("Jimi", "Park",1985)
  val novel = new Novel("Great Travel", 2020, author)

  println(novel.Authorage)
  println(novel.isWrittenBy(imposter))
  println(novel.isWrittenBy(imposter ))
//  val writer = new Writer(firstName = "Jimmy", surName = "Park", yearBirth = 1985)
//  writer.Fullname("Jimmy","Park")
//  val copy = new Novel(yearRelease = 2020, name = "Doo's world", author = "Doo")
//  println(copy.yearRelease)
  
}

//class Person(name: String, val age: Int) // class constructor
//class Person(name: String, age: Int) // class constructor // age is not member

// class parameters are not fields

class Person(name: String, val age: Int) {
  // body
  val x = 2
  println(1+3)
  def greet(name: String): Unit = println(s"${this.name} says: Hi, $name")

  // overloading
  def greet(): Unit = println(s"Hi, I am $name")

  // multiple constructors
  def this(name: String) = this(name, 0)
  def this() = this("John Doe")
}

class Writer(firstName: String, surName: String, val yearBirth: Int) {
//  def Fullname(firstName: String, lastName: String): Unit = println(s"Writer full name is $firstName $lastName ")
  def FullName: String = firstName + " " + surName
}

class Novel(name: String, val yearRelease: Int, author:Writer){
  def Authorage = (2020 - author.yearBirth)
//  def isWrittenBy(author:String): Unit = println(s"Written by : $author")
  def isWrittenBy(author: String) = author == this.author
  def copy(newYear: Int): Novel = new Novel(name, newYear, author)
}

class Counter(current: Int, increment:Int, decrement:Int){
//  val x = 2
  def CurrentCount(): Unit = println(s"Current count is : $current")
  def Increment(current:Int, increment:Int): Unit = println(s"Increment count as $increment current is : ${current+increment}")
  def Increment(): Unit = println(s"Increment result ${current+increment}")

  def Decrement(current:Int, decrement:Int): Unit  = println(s"Decrement count as $z current is : ${current-decrement}")
  def Decrement(): Unit = println(s"Decrement result is : ${current-decrement}")

}

/*
Novel and Writer

Writter = first name, surname, year
 - method fullname

Novel : name, year of release, author
 - authorAge
 - isWrittenBy(author)
 - copy (new year of release) = new instance of novel
 */

/*
  Counter class
    - receives an int value
    - method current count
    - method to increment/decrement => new Counter
    - overload inc/dec to receive an amount
 */
